Param(
  [Parameter(Mandatory=$true)]
  [string]$setupConnString,
  [Parameter(Mandatory=$true)]
  [string]$dbName,
  [Parameter(Mandatory=$true)]
  [string]$dbAppUsername,
  [Parameter(Mandatory=$true)]
  [string]$dbAppPassword
)

Write-Host "Running database migrations" -foregroundcolor "green"
#Ensure the db exists, otherwise create it.
Add-Type -Path (Resolve-Path "./dbup.dll")
$forDB = [DbUp.EnsureDatabase]::For
[SqlServerExtensions]::SqlDatabase($forDB, $setupConnString)

# Run the Up scripts
$dbUp = [DbUp.DeployChanges]::To
$dbUp = [SqlServerExtensions]::SqlDatabase($dbUp, $setupConnString)
$dbUp = [StandardExtensions]::WithScriptsFromFileSystem($dbUp, (Resolve-Path "./sql/up"))
$dbUp = [StandardExtensions]::LogToConsole($dbUp)
$upgradeResult = $dbUp.Build().PerformUpgrade()

# Run the every time scripts
$dbUp = [DbUp.DeployChanges]::To
$dbUp = [SqlServerExtensions]::SqlDatabase($dbUp, $setupConnString)
$dbUp = [StandardExtensions]::WithScriptsFromFileSystem($dbUp, (Resolve-Path "./sql/everytimeScripts"))
$nullJournal = new-object -TypeName DbUp.Helpers.NullJournal
$dbUp = [StandardExtensions]::JournalTo($dbUp, $nullJournal)
$dbUp = [StandardExtensions]::LogToConsole($dbUp)
$upgradeResult = $dbUp.Build().PerformUpgrade()