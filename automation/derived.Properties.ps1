DerivedProperties {
    $slnDir = Resolve-Path(Join-Path $psake.build_script_dir "..\")
    $webSlnFile = Join-Path $slnDir "sample.sln"

    $sourceDir = Join-Path $slnDir "src"
    $webProjectFile = Join-Path $sourceDir "sample/sample.csproj"
    $sqlDir = Join-Path $slnDir "sql"
    $dbUpScriptsDir = Join-Path $sqlDir "up"
    $everytimeScriptsDir = Join-Path $sqlDir "everytimeScripts"
    $buildOutputDir = Join-Path $slnDir "_buildOutput"
    $webOutputDir = Join-Path $slnDir "_webOutput"
    $nugetPackagesDir = Join-Path $slnDir "_nugetPackages"
    $dbPackageDir = Join-Path $slnDir "_dbScripts"
    $packageDir = Join-Path $slnDir "_package"
    $nodeModulesDir = Join-Path (Get-Item $webProjectFile).DirectoryName "/node_modules"
    $packageZip = "${slnDir}\_${env}_package.zip"
    $webPackageZip = "${slnDir}\_${env}_webPackage.zip"

    $nodeZipDownloadFile = "${Env:\TEMP}\nodev${nodeVersion}.zip"
    $nodeZipDownloadUrl = "http://nodejs.org/dist/v${nodeVersion}/node-v${nodeVersion}-win-x64.zip"
    $nodeDownloadPath = "${Env:\TEMP}"
    $nodeExePath = "${Env:\TEMP}\node-v${nodeVersion}-win-x64\"
    $nodeExe = "${Env:\TEMP}\node-v${nodeVersion}-win-x64\node.exe"

    $nugetDownloadPath = $env:TEMP
    $nugetDownloadUrl = "https://dist.nuget.org/win-x86-commandline/v${nugetVersion}/NuGet.exe"
    $nugetExePath = "${Env:\TEMP}\nugetv${nugetVersion}\"
    $nugetExe = "${nugetExePath}nuget.exe"

    $dotnetInstallDir = Join-Path (Resolve-Path ~) "dotnet-${dotnetcliVersion}"
    $dotnetInstallPS = (Join-Path $dotnetInstallDir dotnet-install.ps1)
    $dotnetExe = (Join-Path $dotnetInstallDir dotnet.exe)

    Add-Type -Assembly System.Data
    $appDbconnStringObject = new-object System.Data.SqlClient.SqlConnectionStringBuilder -ArgumentList @($dbAppConnectionString)
    $dbAppUsername = $appDbconnStringObject.UserID
    $dbAppPassword = $appDbconnStringObject.Password
    $appDbName = $appDbconnStringObject.InitialCatalog

    $setupDbconnStringObject = new-object System.Data.SqlClient.SqlConnectionStringBuilder -ArgumentList @($dbSetupConnectionString)
    $dbSetupUsername = $setupDbconnStringObject.UserID
    $dbSetupPassword = $setupDbconnStringObject.Password
    $setupDbName = $setupDbconnStringObject.InitialCatalog

    $dbSetupScript = Join-Path $sqlDir "dbSetup.ps1"
    $dbupDll = Join-Path $nugetPackagesDir "dbup.3.3.5\lib\net35\dbup.dll"
}
