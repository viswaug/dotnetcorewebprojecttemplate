﻿$config.buildFileName="default.ps1"
$config.framework = "4.7"
$config.verboseError=$false
$config.coloredOutput = $true
$config.modules = (".\modules\*.psm1")
$config.taskNameFormat= { param($taskName) "Executing $taskName at $(get-date)" }


