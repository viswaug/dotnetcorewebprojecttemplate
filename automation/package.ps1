Include (Resolve-Path './default.ps1').Path

task cleanWebPackage -depends writeAllProperties {
    if(Test-Path $packageZip)
    {
        Write-Host "Deleting web package- '$webOutputDir'" -foregroundcolor "green"
        Remove-Item -Force -Path $packageZip
    }
}

task packageWeb -depends cleanWebPackage {
    Exec { dotnet publish $webProjectFile -o $webOutputDir -r $runtime }
}

task cleanDBPackage -depends writeAllProperties {
    Write-Host "Deleting DB package directory - '$dbPackageDir'" -foregroundcolor "green"
    Delete $dbPackageDir
}

task initDBPackage -depends cleanDBPackage {
    Write-Host "Creating DB output directory - '$dbPackageDir'" -foregroundcolor "green"
    New-Item $dbPackageDir -type directory
}

task packageDB -depends initDBPackage {
    # Copy-Item -Path $sqlDir -Destination $dbPackageDir -Force -Recurse
}

task package -depends build, packageDB, packageWeb {
    if(Test-Path $packageDir)
    {
        Write-Host "Deleting production package directory - '$packageDir'" -foregroundcolor "green"
        Delete $packageDir
    }
    New-Item $packageDir -type directory

    Copy-Item -Path $webOutputDir -Destination $packageDir -Force -Recurse
    Copy-Item -Path $dbPackageDir -Destination $packageDir -Force -Recurse

    Write-Host "Creating $packageZip with production package subdirectories." -foregroundcolor Green
    Add-Type -assembly "system.io.compression.filesystem"
    if(Test-Path $packageZip) {
        Write-Host "Deleting existing production pacakge file $packageZip'." -ForegroundColor Green
        Remove-Item "${slnDir}\${env}_package.zip"
    }
    [io.compression.zipfile]::CreateFromDirectory("$packageDir", "${packageZip}")
}
