Function CreateAppUserInDatabase([String]$setupConnString, [String]$dbName, [String]$dbAppUsername, [String]$dbAppPassword) {
    Add-Type -Assembly System.Data
    $conn = New-Object System.Data.SqlClient.SqlConnection -ArgumentList @($setupConnString)
    $query = @"
IF EXISTS (SELECT * FROM sys.server_principals WHERE name = N'$dbAppUsername') AND EXISTS (SELECT * FROM sys.database_principals WHERE name = N'$dbAppUsername')
RETURN;

-- DROP USER LOGIN FROM SERVER
IF NOT EXISTS (SELECT * FROM sys.server_principals WHERE name = N'$dbAppUsername')
CREATE LOGIN [$dbAppUsername] WITH PASSWORD=N'$dbAppPassword', DEFAULT_DATABASE=[$dbName], DEFAULT_LANGUAGE=[us_english], CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF
;
-- DROP THE USER IN THE DB
USE [$dbName]

IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'$dbAppUsername')
BEGIN
    USE [$dbName]
    CREATE USER [$dbAppUsername] FOR LOGIN [$dbAppUsername] WITH DEFAULT_SCHEMA=[dbo]
END
;

exec sp_addrolemember 'db_datawriter', $dbAppUsername
exec sp_addrolemember 'db_datareader', $dbAppUsername
;

-- SYNC THE LOGINS
USE [$dbName]
exec sp_change_users_login 'update_one', '$dbAppUsername', '$dbAppUsername'
;
GRANT EXECUTE TO $dbAppUsername
;
"@
    
    $cmd = New-Object System.Data.SqlClient.SqlCommand -ArgumentList($query, $conn)
    $conn.Open();
    $cmd.ExecuteNonQuery();
    $conn.Close();
}

function Delete([String]$path) {
    if (Test-Path $path) {
        # Have to do this craziness because Remove-Item -Recurse is broken
        Get-ChildItem $path -recurse |
            Foreach-Object { $_.FullName } |
            Sort-Object -descending |
            Foreach-Object {
                Remove-Item $_ -Force
                # The 0 second sleep slows down the delete process just enough to avoid weird error where Remove-Item returns before deleting stuff
                Start-Sleep -Milliseconds 0
            }
        if (Test-Path $path) { Remove-Item $path }
    }
}

function Merge-Tokens($template, $tokens)
{
    return [regex]::Replace(
        $template,
        '\$(?<tokenName>\w+)\$',
        {
            param($match)
            $tokenName = $match.Groups['tokenName'].Value
            return $tokens[$tokenName]
        })
}


Function Expand-Tokens {
    Param([Parameter(Mandatory=$true)][System.IO.FileInfo]$templateFile,
          [System.IO.FileInfo]$destConfig=(join-path $templateFile.Directory.FullName ($templateFile -replace ".template$", ""))
         )

    $contents = [IO.File]::ReadAllText($templateFile.FullName)
    #escape out the ', " and the ` characters in the string because the ExpandString method will strip em
    $single, $double, $grave = "'", '"', "``"
    $contents = $contents -replace "($single|$double|$grave)", '`$1'
    $contents = $contents -replace '\$\$', '`$'
    #$ExecutionContext.InvokeCommand.ExpandString($contents) | out-file $destConfig

    # Had to switch how we're outputing config files because out-file always uses a byte order mark
    # when UTF8 is the encoding, and nose-testconfig doesn't like byte order marks
    $utf8NoBomEncoding = New-Object System.Text.UTF8Encoding($False)
    [System.IO.File]::WriteAllText($destConfig, $ExecutionContext.InvokeCommand.ExpandString($contents), $utf8NoBomEncoding)
}

Function Expand-AllTemplateFiles([Parameter(Mandatory=$true)]$baseDir) {
    $templateFiles = Get-ChildItem -Path $baseDir -Filter *.template -Recurse
    if($templateFiles -is [System.IO.FileInfo]) { Expand-Tokens($templateFiles); return }
    else {
        Foreach($templateFile in $templateFiles) { Expand-Tokens($templateFile) }
    }
}

Function Run-Tests {
    Param ([Parameter(Mandatory=$true)][System.IO.FileInfo]$testRunner,
           [Parameter(Mandatory=$true)][System.IO.FileInfo[]]$testFileNames,
           [Parameter(Mandatory=$true)][string]$testResultFile,
           [Parameter(Mandatory=$true)][string]$testOutputFile,
           [Parameter(Mandatory=$true)][string]$testErrorOutputFile)

    Write-Host "$testRunner $testFileNames /nologo /result=$testResultFile" -foregroundcolor "green"
    Exec { & $testRunner $testFileNames /nologo /result=$testResultFile /out=$testOutputFile /err=$testErrorOutputFile }
}

export-modulemember -function Delete, CreateAppUserInDatabase, Expand-Tokens, Expand-AllTemplateFiles, Merge-Tokens, Run-Tests
