
Param (
     [Parameter(Mandatory=$True, HelpMessage="Name of IIS application pool")] [string]$appPoolName = "sample",
     [Parameter(Mandatory=$True, HelpMessage="Folder path for the IIS website")] [string]$sitePath = "C:\inetpub\sample",
     [Parameter(Mandatory=$True, HelpMessage="Name of the IIS website")] [string]$siteName = "sample",
     [Parameter(Mandatory=$True, HelpMessage="Host name for the HTTPS binding")] [string]$hostHeader = "",
     [Parameter(Mandatory=$True, HelpMessage="Common name of the SSL certificate to be used for the HTTPS binding. The certificate must already exist in the certificate store")] [string]$certCommonNameRegex = ""
)

Import-Module WebAdministration
$ErrorActionPreference = "Stop"

Write-Host "Installing IIS" -ForegroundColor Cyan
Install-WindowsFeature Web-Server

#Create ApplicationPool
if(!(Test-Path IIS:\AppPools\$appPoolName)) {
    Write-Host "Creating application pool '$appPoolName'" -ForegroundColor Cyan
    New-WebAppPool $appPoolName
}
else { Write-Host "Application pool '$appPoolName' already exists." -ForegroundColor Green}

#Create Website folder
if(!(Test-Path $sitePath)) {
    Write-Host "Creating website folder path '$sitePath'." -ForegroundColor Cyan
    New-Item -Path $sitePath -ItemType directory -Force
    $appPoolUser = "IIS AppPool\$appPoolName"
    $acl = Get-Acl $sitePath      
    $ar = New-Object System.Security.AccessControl.FileSystemAccessRule($appPoolUser, "FullControl", "ContainerInherit,ObjectInherit", "None", "Allow")
    $acl.AddAccessRule($ar)
    Write-Host "Assigning ACL permissions for application pool user on website folder." -ForegroundColor Cyan
    Set-Acl $sitePath $acl   
}
else { Write-Host "Website folder '$sitePath' already exists." -ForegroundColor Green }

#Create Website
if(!(Test-Path IIS:\Sites\$siteName)) {
    Write-Host "Creating Website '$siteName'" -ForegroundColor Cyan
    New-WebSite -Name $siteName -Port 80 -HostHeader $hostHeader -PhysicalPath $sitePath
}
else { Write-Host "Website '$siteName' already exists." -ForegroundColor Green}

#Create WebBinding
$binding = Get-WebBinding -Name $siteName -Protocol https -Port 443 -IPAddress "*" -HostHeader $hostHeader
if($binding -eq $null) {
    $thumbprint = Get-ChildItem -Path  Cert:\LocalMachine\My | Where-Object {$_.subject -match "^CN=$certCommonNameRegex"} |  Select-Object -ExpandProperty Thumbprint
    Write-Host "Identified certificate with thumbprint '$thumbprint' for use." -ForegroundColor Cyan
    Write-Host "Creating HTTPS binding for '$siteName'" -ForegroundColor Cyan
    New-WebBinding -Name $siteName -Protocol https -Port 443 -IPAddress "*" -HostHeader $hostHeader #-SslFlags 1
    if(Test-Path IIS:\SslBindings\0.0.0.0!443) {
        Write-Host "Deleting existing SSL binding" -ForegroundColor Cyan
        Get-ChildItem IIS:\SslBindings\0.0.0.0!443 | Remove-Item
    }
    Write-Host "Creating new SSL binding" -ForegroundColor Cyan
    Get-Item -Path  "cert:\localmachine\my\$thumbprint" | New-Item -Path IIS:\SslBindings\0.0.0.0!443
}
else {Write-Host "HTTPS binding for site '$siteName' already exists." -ForegroundColor Green}
