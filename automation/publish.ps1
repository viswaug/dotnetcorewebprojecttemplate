
Include (Resolve-Path './package.ps1').Path

task publish -depends packageWeb {
    if(!$webHostPSRemotingCreds) {
        $webHostPSRemotingCreds = Get-Credential
    }
    $soptions = New-PSSessionOption -SkipCACheck
    $pssession = New-PSSession -ComputerName $webHostName -Port $winrmPort -Credential $webHostPSRemotingCreds -SessionOption $soptions -UseSSL

    $zipAtDest = (Join-Path $sitePath "mainecs.zip")
    if(Test-Path $webPackageZip) {
        Remove-Item $webPackageZip -Force
    }
    Compress-Archive -Path $webOutputDir\* -DestinationPath $webPackageZip -Force
    Copy-Item -Force -ToSession $pssession -Recurse -Path $webPackageZip -Destination $zipAtDest

    Invoke-Command -Session $pssession -FilePath (Join-Path (Split-Path -Parent $PSCommandPath) bootstrap.ps1) -ArgumentList $appPoolName, $sitePath, $siteName, $hostHeader, $certCommonNameRegex

    Invoke-Command -Session $pssession -ScriptBlock {
    Param (
        [Parameter(Mandatory=$True, HelpMessage="The zip file to extract to the destination")] [string]$zip,
        [Parameter(Mandatory=$True, HelpMessage="Zip file content destination path")] [string]$dest
    )
        Get-ChildItem -Path $sitePath -Include *.* -File -Recurse -Exclude (Split-Path -Leaf $zip) | ForEach-Object { $_.Delete()}
        [System.Reflection.Assembly]::LoadWithPartialName('System.IO.Compression.FileSystem')
        [System.IO.Compression.ZipFile]::ExtractToDirectory($zip, $dest)
        Remove-Item $zip
    } -ArgumentList $zipAtDest, $sitePath
}