#############################################################
#Declare default values for the properties that are applicable (which in almost all cases should be all properties)
#These properties will be overridden by property values set in local and machine specific properties files
#############################################################
$defaultPropertiesFilePath = ".\default.properties.ps1"
if(Test-Path $defaultPropertiesFilePath) {
    Write-Host "Including file $defaultPropertiesFilePath" -foregroundcolor "green"
    Include (Resolve-Path $defaultPropertiesFilePath).Path
}

##############################################################
#Include any local and machine specific properties file that are found
#These included properties will be overridden by property values passed in to the script via the commandline
##############################################################
$localPropertiesFilePath = ".\local.properties.ps1"
if(Test-Path $localPropertiesFilePath) {
    Write-Host "Including file $localPropertiesFilePath" -foregroundcolor "green"
    Include (Resolve-Path $localPropertiesFilePath).Path
}

$machinePropertiesFilePath = ".\${env}.properties.ps1"
if(Test-Path $machinePropertiesFilePath) {
    Write-Host "Including file $machinePropertiesFilePath" -foregroundcolor "green"
    Include (Resolve-Path $machinePropertiesFilePath).Path
}

##############################################################
#Include file were derived properties are calculated
##############################################################
if(Test-Path ".\derived.Properties.ps1") {
    Write-Host "Including file $((Resolve-Path ".\derived.Properties.ps1").Path)" -foregroundcolor "green"
    Include (Resolve-Path ".\derived.Properties.ps1").Path
}

###############################################################
#All tasks begin here
###############################################################
task writeAllProperties `
    -Description "Outputs all the properties used by the build script" `
{
    $props = & $psake.get_variables
    Format-Table -Wrap -AutoSize -Property @("Name", "Value") -InputObject $props | Out-string
}


task cleanNpmModules `
    -Description "Deletes all the downloaded NPM modules" `
{
    Write-Host "Deleting node_modules directory - '$nodeModulesDir'" -foregroundcolor "green"
    Delete "$nodeModulesDir"
}

task cleanBuildOutput `
    -Description "Deletes all the output produced by the build, package, test tasks. Does not clean the nuget packages and the node modules" `
    -depends writeAllProperties `
{
    Write-Host "Cleaning build output directory - '$buildOutputDir'" -foregroundcolor "green"
    Delete $buildOutputDir
    Write-Host "Cleaning web output directory - '$webOutputDir'" -foregroundcolor "green"
    Delete $webOutputDir
    Write-Host "Cleaning DB scripts directory - '$dbPackageDir'" -foregroundcolor "green"
    Delete $dbPackageDir
    Write-Host "Cleaning package directory - '$packageDir'" -foregroundcolor "green"
    Delete $packageDir
    Write-Host "Cleaning package file - '$packageZip'" -foregroundcolor "green"
    Delete $packageZip
    Delete $webPackageZip
    # Delete $nugetPackagesDir
}

task clean `
    -description "same as the cleanBuildOuput task" `
    -depends cleanBuildOutput { }

task cleanAll `
    -description "Deletes downloaded nuget packages, node modules, build output, test results and web output." `
    -depends cleanNpmModules, clean { }

task bootstrapDotNet `
    -description "Downloads dotnet to temporary location on machine if not already present" `
{
    if(-Not (Test-Path $dotnetInstallDir)) {
        New-Item -ItemType Directory -Path $dotnetInstallDir
    }
    if(-Not (Test-Path $dotnetInstallPS)) {
        Write-Host "Downloading the CLI installer..."
        Invoke-WebRequest `
        -Uri "https://raw.githubusercontent.com/dotnet/cli/v${dotnetcliVersion}/scripts/obtain/dotnet-install.ps1" `
        -OutFile $dotnetInstallPS
    }

    if(-Not (Test-Path $dotnetExe)) {
        Write-Host "Installing the CLI requested version ($dotnetcliVersion) ..."
        & $dotnetInstallPS -Version $dotnetcliVersion -InstallDir $dotnetInstallDir
        Write-Host "Downloading and installation of the Dotnet SDK is complete."
    }

    Write-Host "Adding Dotnet cli tools to path" -ForegroundColor Green
    $env:Path = $dotnetInstallDir + ";" + $env:Path
    $version = (& dotnet --version)
    Write-Host "Dotnet cli tools version is $version" -ForegroundColor Green
}

task bootstrapNuget `
    -description "Downloads nuget to temporary location on machine if not already present" `
{
    if(-Not (Test-Path $nugetExe)) {
        if(-Not (Test-Path $nugetExePath)) {
            New-Item $nugetExePath -ItemType Directory
        }
        Write-Host "Downloading Nuget from '$nugetDownloadUrl' to '$nugetExe'" -ForegroundColor Green
        Invoke-WebRequest $nugetDownloadUrl -OutFile $nugetExe
        Write-Host "Done downloading Nuget" -ForegroundColor Green
    }
    $env:Path = $nugetExePath + ";" + $env:Path
    Write-Host "Adding Nuget to path" -ForegroundColor Green
    Write-Host "Nuget version is $nugetVersion" -ForegroundColor Green
}

task bootstrapNode `
    -description "Downloads node and npm to temporary location on machine if not already present" `
{
    if(-Not (Test-Path $nodeZipDownloadFile)) {
        Write-Host "Downloading Node from '$nodeZipDownloadUrl' to '$nodeZipDownloadFile'" -ForegroundColor Green
        Invoke-WebRequest $nodeZipDownloadUrl -OutFile $nodeZipDownloadFile
        Write-Host "Done downloading Node" -ForegroundColor Green
    }
    if(-Not (Test-Path $nodeExe)) {
        Add-Type -AssemblyName System.IO.Compression.FileSystem
        Write-Host "Extracting '$nodeZipDownloadFile' to '$nodeDownloadPath'" -ForegroundColor Green
        [System.IO.Compression.ZipFile]::ExtractToDirectory($nodeZipDownloadFile, $nodeDownloadPath)
        Write-Host "Done extracting Node" -ForegroundColor Green
    }
    $env:Path = $nodeExePath + ";" + $env:Path
    Write-Host "Adding Node to path" -ForegroundColor Green
    Write-Host "Node version is " (& node --version) -ForegroundColor Green
}

task initNugetPackages `
    -description "Downloads nuget packages" `
    -depends bootstrapDotNet, bootstrapNuget `
{
    Write-Host "Restoring nuget packages" -foregroundcolor "green"
    exec {nuget install dbup -version 3.3.5 -o $nugetPackagesDir}
    exec {dotnet restore $webSlnFile}
}

task resetDB `
    -description "Drops the existing database and recreates it from the DB scripts." `
{
    Write-Host "Dropping database" -foregroundcolor "green"

    Add-Type -Assembly System.Data
    $dropDb = $setupDbconnStringObject.InitialCatalog
    # have to use the different syntax below because of the space in the property name
    $setupDbconnStringObject["Initial Catalog"] = "master"
    # or $setupDbconnStringObject.psbase.InitialCatalog = "master"
    $conn = New-Object System.Data.SQLClient.SQLConnection -ArgumentList @($setupDbconnStringObject.ConnectionString)
    $conn.Open()
    $command = $conn.CreateCommand()
    $command.CommandText = "USE MASTER; DROP DATABASE $dropDb;"
    $command.ExecuteNonQuery()
    $conn.Close()

    Invoke-Task initDB
}

task initDB `
    -description "Creates the database if it does not already exist. Runs the database migration scripts against the database" `
    -precondition { $runInitDB } `
{
    Write-Host "Running database migrations" -foregroundcolor "green"
    #Ensure the db exists, otherwise create it.
    Add-Type -Path $dbupDll
    $forDB = [DbUp.EnsureDatabase]::For
    [SqlServerExtensions]::SqlDatabase($forDB, $dbSetupConnectionString)

    if(Test-Path $dbUpScriptsDir) {
        Write-Host "Running 'up' scripts" -foregroundcolor "green"
        $dbUp = [DbUp.DeployChanges]::To
        $dbUp = [SqlServerExtensions]::SqlDatabase($dbUp, $dbSetupConnectionString)
        $dbUp = [StandardExtensions]::WithScriptsFromFileSystem($dbUp, $dbUpScriptsDir)
        $dbUp = [StandardExtensions]::LogToConsole($dbUp)
        $upgradeResult = $dbUp.Build().PerformUpgrade()
    } else {
        Write-Host "Skipping database UP scripts run. No 'Up' folder found in $sqlDir" -ForegroundColor Yellow
    }


    if(Test-Path $everytimeScriptsDir) {
        Write-Host "Running 'everytime' scripts" -foregroundcolor "green"
        $dbUp = [DbUp.DeployChanges]::To
        $dbUp = [SqlServerExtensions]::SqlDatabase($dbUp, $dbSetupConnectionString)
        $dbUp = [StandardExtensions]::WithScriptsFromFileSystem($dbUp, $everytimeScriptsDir)
        $nullJournal = new-object -TypeName DbUp.Helpers.NullJournal
        $dbUp = [StandardExtensions]::JournalTo($dbUp, $nullJournal)
        $dbUp = [StandardExtensions]::LogToConsole($dbUp)
        $upgradeResult = $dbUp.Build().PerformUpgrade()
    } else {
        Write-Host "Skipping database EVERTIME scripts run. No 'everytimescripts' folder found in $sqlDir" -ForegroundColor Yellow
    }

    if($dbSetupConnectionString -and $appDbName -and $dbAppUsername -and $dbAppPassword) {
        Write-Host "Creating application user $dbAppUsername in database" -ForegroundColor Green
        CreateAppUserInDatabase $dbSetupConnectionString $appDbName $dbAppUsername $dbAppPassword
    }
}

task init `
    -description "Cleans the build output and runs database migrations" `
    -depends clean, initDB `
{
    New-Item $buildOutputDir -type directory
}

task config `
    -description "Generates files like configuration files from their corresponding template files. All files ending in '.template' will be processed by replacing property tokens in them and writing out new files without the '.template' extension. For example, a file named 'abc.config.template' will generate another called 'abc.config' with the tokens replaced." `
{
    if($debug) {Invoke-Task writeAllProperties}
    $single, $double, $grave = "'", '"', "``"
    #Replace the tokens in all template files under solution directory
    Get-ChildItem -Path $slnDir -Filter *.template -Recurse | Where-Object{ $_.fullname -notmatch "\\node_modules\\?" } | ForEach-Object {
        Write-Host "Replacing tokens in " $_.fullName -foregroundcolor "green"
        $newFileName = $_.FullName -replace ".template$", ""
        $fileContents = [System.IO.File]::ReadAllText($_.FullName)
        $fileContents = $fileContents -replace "($single|$double|$grave)", '`$1'
        $fileContents = $fileContents -replace '\$\$', '`$'
        $fileContents = '"' + $fileContents + '"'
        $fileContents = [System.Environment]::ExpandEnvironmentVariables($fileContents)
        Invoke-Expression -Command $fileContents | Out-File $newFileName -Encoding ASCII
    }
}

task initNpmPackages `
    -depends bootstrapNode `
{
    $webProjectDir = (Get-Item $webProjectFile).DirectoryName
    $pkgJson = "${webProjectDir}\package.json"
    if(Test-Path $pkgJson) {
        Push-Location $webProjectDir
        Exec { npm install}
        Pop-Location
    } else {
        Write-Host "Skipping NPM INSTALL. '$pkgJson' file was not found" -ForegroundColor Green
    }
}

task gruntBuild `
    -description "Runs the default task in the grunt file located in the root of the website's root directory" `
    -depends bootstrapNode `
{
    $webProjectDir = (Get-Item $webProjectFile).DirectoryName
    $gruntFile = "${webProjectDir}\gruntfile.js"
    if(Test-Path $gruntFile) {
        Push-Location $webProjectDir
        $tasks = $(ConvertTo-Json $gruntTasks -Compress)
        $options = $(ConvertTo-Json $gruntOptions -Compress)
        $double = '"'
        $gruntCommand = "require('grunt').tasks($tasks, $options);" -replace "($double)", '"$1'
        Exec { node -e $gruntCommand }
        Pop-Location
    } else {
        Write-Host "Skipping Grunt run. '$gruntFile' file was not found" -ForegroundColor Green
    }
}

task build `
    -description "Builds the website after generating the configuration files" `
    -depends init, compile `
{}

task compile `
    -description "Builds the website after generating the configuration files" `
    -depends config, initNugetPackages `
{
    Exec { dotnet build $webSlnFile -o $buildOutputDir}
}

task rebuild `
    -description "Initializes the nuget packages and node modules. Runs the grunt build script and builds and tests the website" `
    -depends initNugetPackages, initNpmPackages, gruntBuild, build

task default `
    -description "Calls the Test task" `
    -depends build
