#############################################################
#Declare default values for the properties that are applicable (which in almost all cases should be all properties)
#These properties will be overridden by property values set in local and machine specific properties files
#############################################################
properties {
    $debug = $false # this applies to debugging the build script itself not for the project build configuration type
    $buildConfiguration = "Release" # this applies to the build configuration with which all the projects will be built
    $runtime = "win7-x64"
    $msbuildVerbosity = "Minimal"
    $msbuildCpuCount = [System.Environment]::ProcessorCount
    $msbuildParralel = $true
    $runInitDB = $true
    $publishUsername = "username"
    $publishPassword = "PASSWORD"

    $gruntTasks = @("default")
    $gruntOptions = @{"verbose" = $false}
    
    $nodeVersion = "6.11.0"
    $nugetVersion = "3.5.0"
    $dotnetcliVersion = "1.0.4"

    # this applies to versioning the assemblies produced by the build
    $majorVersion = 1
    $minorVersion = 0
    $patchVersion = 0
    $buildVersion = 00000
    
    $dbSetupConnectionString = "Data Source=.\SQLEXPRESS;Initial Catalog=mainecs;Integrated Security=SSPI;"
    $dbAppConnectionString = "Data Source=.\SQLEXPRESS;Initial Catalog=mainecs;Integrated Security=SSPI;"

    #Deployment variables
    $webHostName = "localhost"
    $winrmPort = "5986"
    $appPoolName = "sample"
    $sitePath = "c:\inetpub\sample"
    $siteName = "sample"
    $hostHeader = ""
    $certCommonNameRegex = ""
    $webHostPSRemotingCreds = $null
}
